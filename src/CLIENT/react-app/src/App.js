import "./App.css";
import io from "socket.io-client";
import { useState } from "react";
import Chat from "./Chat";



const socket = io.connect("http://localhost:3001");

function Main() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [selectedContact, setSelectedContact] = useState("");
  const [showChat, setShowChat] = useState(false);
  const [loginError, setLoginError] = useState(false);
  const [selectedContactname, setSelectedContactName] = useState("");
 

  const contactList = [
    {
      name: "Julie Rimas",
      id: 1
    },
    {
      name: "Intoy Arreza",
      id: 2
    },
    {
      name: "Bob Myer",
      id: 3
    },
    {
      name: "Michael Jordan",
      id: 4
    },
    {
      name: "Lebron James",
      id: 5
    },
    {
      name: "Stephen Curry",
      id: 6
    },
  ];


  const chats = () => {
    // Mock database for username and password

    const mockUsername = "virgo";
    const mockPassword = "admin123";

    const mockUsername2 = "client";
    const mockPassword2 = "admin123";


    const selectedContactObj = contactList.find(contact => contact.id === parseInt(selectedContact));
    setSelectedContactName(selectedContactObj.name)
 
    if (username === mockUsername && password === mockPassword && selectedContactObj) {
      socket.emit("JoinRoom", selectedContactObj.id);
      setShowChat(true);
    } 
    else if (username === mockUsername2 && password === mockPassword2 && selectedContactObj) {
      socket.emit("JoinRoom", selectedContactObj.id);
      setShowChat(true);

    }
    
    else {
      setLoginError(true);
    }
  };
  

  return (
    <div className="App">
      {!showChat ? (
        <div className="ChatContainer">
          <h3>MESSENGER</h3>

          <input
            type="text"
            placeholder="Your Username"
            onChange={(event) => {
              setUsername(event.target.value);
            }}
          />

          <input
            type="password"
            placeholder="Your Password"
            onChange={(event) => {
              setPassword(event.target.value);
            }}
          />

          <div className="ContactList">
            <h4>Select a contact:</h4>
            <select value={selectedContact} onChange={(event) => setSelectedContact(event.target.value)}>
            
              <option value="">--Select a contact--</option>
              {contactList.map(contact => (
                <option key={contact.id} value={contact.id}>
                  {contact.name}
                </option>
              ))}
            </select>
          </div>

          {loginError && <p>Invalid username or password</p>}
          <button onClick={chats}>Message</button>
        </div>
      ) : (
        <Chat socket={socket} username={username} id={selectedContact} name={selectedContactname}/>
      )}
    </div>
  );
}

export default Main;
