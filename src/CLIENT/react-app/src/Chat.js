import React, { useEffect, useState } from "react";
import ScrollToBottom from "react-scroll-to-bottom";


function Chat({ socket, username, id, name }) {
  const [currentMessage, setCurrentMessage] = useState("");
  const [selectedFile, setSelectedFile] = useState(null);
  const [messageList, setMessageList] = useState([]);

  const handleFileSelect = (event) => {
    setSelectedFile(event.target.files[0]);
  };

  const sendMessage = async () => {
    if (currentMessage !== "") {
      const messageData = {
        id: id,
        author: username,
        message: currentMessage,
        time:
          new Date(Date.now()).getHours() +
          ":" +
          new Date(Date.now()).getMinutes(),
        file: selectedFile,
      };

      await socket.emit("send_message", messageData);
      setMessageList((list) => [...list, messageData]);
      setCurrentMessage("");
      setSelectedFile(null);
    }
  };

  useEffect(() => {
    socket.on("receive_message", (data) => {
      setMessageList((list) => [...list, data]);
    });
  }, [socket]);


  const logout = () => {
    window.location.reload(); 
  };

  return (
    <div className="ChatWindow">
      <div className="ChatHeader">
        <h3>Message to {name}</h3>
      </div>
      <div className="ChatBody">
        <ScrollToBottom className="message-container">
          {messageList.map((messageContent) => {
            return (
              <div
                className="message"
                id={username === messageContent.author ? "you" : "other"}
              >
                <div>
                  {messageContent.file ? (
                    <div className="message-content">
                      <p>{messageContent.message}</p>
                      <a href={messageContent.file} download>
                        Download File
                      </a>
                    </div>
                  ) : (
                    <div className="message-content">
                      <p>{messageContent.message}</p>
                    </div>
                  )}
                  <div className="message-meta">
                    <p id="time">{messageContent.time}</p>
                    <p id="author">{messageContent.author}</p>
                  </div>
                </div>
              </div>
            );
          })}
        </ScrollToBottom>
      </div>
      <input type="file" accept=".jpg,.jpeg,.png,.gif,.pdf" onChange={handleFileSelect} />
      <div className="ChatFooter">
        <input
          type="text"
          value={currentMessage}
          placeholder=" . . . "
          onChange={(event) => {
            setCurrentMessage(event.target.value);
          }}
          onKeyPress={(event) => {
            event.key === "Enter" && sendMessage();
          }}
        />
        
        <button onClick={sendMessage}>Send</button>

      </div>
      <div><button onClick={logout}>Logout</button> </div>
    </div>
  );
}

export default Chat;
